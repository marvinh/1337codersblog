March 7, 2017

Categories: Circuit Design, Hardware

Tags: Capacitor, Diode, MOSFET, 
Priorized Voltage Source Switch, Transistor

# Priorized Voltage Source Switch
	

I have been asked by a workmate if I could design 
a circuit which dynamically switches to another 
voltage source if the voltage of the primary source 
has dropped. He wants to install Solarpanels and 
would like to have a circuit which primarly uses the 
batteries being loaded by solar panels, for powering 
some lightening, but when the batteries are low, the 
power should be taken from the energy network. So I 
did some google research and some thinking and finally 
got the following circuit:

![Schematics](../resources/priorized-voltage-source-switch.svg)

The red bordered area represents the batteries 
(the capacitor on the left) and the solar panels 
(the 12 Volt voltage source on the right and the 
switch on the top, simulating the sun shining on the 
solar panels). The 12 Volt voltage source left next to 
the red bordered area represents the power from the energy 
network after it has been transformed down to 12 Volt and 
having passed a rectifier. When the batteries are loaded, 
the P-Channel MOSFET is turned off, so the power is taken 
from the batteries. The diode which is passed by the current 
from the batteries, prevents the current to flow from 
the energy net to the batteries, so the batteries only
 will be loaded by the solar panels.

The part on the left (consisting of the bipolar transistor, 
the base resistance, the TVS diode and the consumer resistance) 
is a simple voltage regulator circuit as it can be found on 
[wikipedia](https://de.wikipedia.org/wiki/Spannungsregler). The TVS diode of the circuit limits the voltage of 
the consumer to a fixed value. So if you choose a TVS diode 
with a breakthrough voltage of 12 Volt, the consumer will get 
a maximum voltage of 12 Volt. The bipolar transistor regulates 
the current flow in a way that it will be constant for the consumer.

When the voltage of the batteries drop, the P-Channel 
MOSFET will turn on, so the reduce in voltage and current 
flow will be compensated by the energy network. The lower 
the battery get, the more power will be taken from the energy 
network. In my simulation, the voltage of the batteries dropped 
from 12 Volt to ~9 Volt, then no more power was taken from the 
batteries and all power flowed from the energy network.

[>>previous entry](The PathStar-Algorithm.md)