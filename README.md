# Copyright note and licensing information
Copyright &copy; 1337coder, 09.04.2017<br>
This work is licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License](LICENSE.txt).

# Purpose
This is a downloadable and printable copy of the [1337codersblog](1337codersblog.wordpress.com). It will be updated each 
time a new blog entry is published.